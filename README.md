Edison_Mezzanine_9DOF
=====================

The 9DOF board for the Edison uses the STMicro LSM9DS0 9 Degrees of Freedom IMU for full-range motion sensing. This chip combines a 3-axis accelerometer, a 3-axis gyroscope, and a 3-axis magnetometer. By default, the IMU is connected to the Edison through the I2C bus but can be configured to use the SPI bus.
